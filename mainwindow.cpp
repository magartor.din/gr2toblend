#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <unistd.h>

#include <QFileDialog>
#include <QFile>
#include <QProcess>
//#include <QDebug>

extern int newIndicator;
extern int freeIndicator;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    exporterObj = new Exporter;
    m_animFiles.clear();
    m_animName.clear();
    connect(exporterObj, &Exporter::createNa2File, this, &MainWindow::on_ExporterNewNa2File);
    connect(exporterObj, &Exporter::createCn6File, this, &MainWindow::on_ExporterNewCn6File);
}

void MainWindow::on_ExporterNewNa2File(std::string fileName){
    m_listNa2File.push_back(fileName);
}
void MainWindow::on_ExporterNewCn6File(std::string fileName){
    m_listCn6File.push_back(fileName);
}

MainWindow::~MainWindow(){
    delete exporterObj;
    delete ui;
}
void MainWindow::on_pushButton_Open_clicked(){                              //grannyFileExtension
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", tr("gr2 file (*.gr2 *.GR2)"));
    if(fileName == ""){
        return;
    }
    //Démarre l'exporteur
    if(exporterObj->m_grannyFile != nullptr){
        GrannyFreeFile(exporterObj->m_grannyFile);
        exporterObj->m_grannyFile = nullptr;
    }
    exporterObj->loadedFilename = fileName.toStdString();
    std::string Filename(exporterObj->loadedFilename);

    exporterObj->m_grannyFile =   exporterObj->openFile(Filename);
    exporterObj->m_fileInfo =  exporterObj->onFileOpen(exporterObj->m_grannyFile);
    if((exporterObj->m_grannyFile != nullptr)&&(exporterObj->m_fileInfo != nullptr)){
        ui->pushButton_exportCN6->setEnabled(true);
        ui->pushButton_ExportAnimNA2->setEnabled(true);
        ui->pushButton_ExportAllModelNB2->setEnabled(true);
        ui->pushButton_OpenAnimTxt->setEnabled(true);
    }
}

void MainWindow::on_pushButton_ExportAllModelNB2_clicked(){
    if (exporterObj->m_fileInfo == nullptr) {
        //printf("Could not get fileinfo\n");
        //exit(1);
    }
    else{
        exporterObj->exportNB2AllModel(exporterObj->m_fileInfo);
    }
}

void MainWindow::on_pushButton_ExportAnimNA2_clicked(){                       //grannyFileExtension
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", tr("gr2 file (*.gr2 *.GR2)"));
    if(fileName == ""){
        return;
    }
    if(exporterObj->m_animFileInfo != nullptr){
        GrannyFreeFile(exporterObj->m_grannyAnimFile);
        exporterObj->m_animFileInfo = nullptr;

    }
    exporterObj->loadedAnimFile = fileName.toStdString();
    std::string AnimFileName(exporterObj->loadedAnimFile);

    exporterObj->m_grannyAnimFile = exporterObj->openFile(AnimFileName);
    exporterObj->m_animFileInfo =  exporterObj->onAnimFileOpen(exporterObj->m_grannyAnimFile);
    if (exporterObj->m_animFileInfo == nullptr) {
        //printf("Could not get fileinfo\n");
        //exit(1);
    }
    else{
        exporterObj->exportNA2Anim(exporterObj->m_animFileInfo, 0, exporterObj->m_fileInfo);
    }
}

void MainWindow::on_pushButton_exportCN6_clicked()
{
    exporterObj->exportAllModelsToCN6(exporterObj->m_fileInfo, false);
}

std::string MainWindow::getFileNameFromPath(std::string str){
    int i;
    if(str.size()==0)
        return std::string("");
    for(i = str.size()-1; (str.at(i) != '\\')&&(str.at(i) != '/')&&(i>0); i--);
    return std::string(str.substr(i+1, str.size()-i));
}
std::string MainWindow::getFolderFromPath(std::string str){
    int i;
    if(str.size()==0)
        return std::string("");
    for(i = str.size()-1; (str.at(i) != '\\')&&(str.at(i) != '/')&&(i>0); i--);
    return std::string(str.substr(0, i+1));
}
bool MainWindow::readMsmFile(std::string fileName){
    QFile msmFile(QString::fromStdString(fileName));

    if(msmFile.open(QIODevice::ReadOnly | QIODevice::Text) == false){
        //Handle error
        return false;
    }
    //File opened
    //Looking for BaseModelFileName

    std::string gr2PathFile("");
    bool fieldFound = false;
    while (!msmFile.atEnd()) {
        //Get new line
        QByteArray line = msmFile.readLine();
        char * lineData = line.data();
        //Check it's line we are looking for
        if(strncmp(lineData, "BaseModelFileName", strlen("BaseModelFileName")) != 0){
            //It's not so gonext line
            continue;
        }
        // Good line found
        int indexLine = 0;
        int length = line.length();
        //Skip BaseModelFileName
        while((lineData[indexLine] != '\"')&&(indexLine < length)){
            indexLine++;
        }
        indexLine++;
        //Get content of " caracters
        while((lineData[indexLine] != '\"')&&(indexLine < length)){
            gr2PathFile.push_back(lineData[indexLine++]);
        }
        //Break when finished
        fieldFound = true;
        break;
    }
    //Check if field found
    msmFile.close();
    if(!fieldFound){
        //Field not found quit
        return false;
    }
    //Field found check if file exist

    std::string gr2FolderFile(getFolderFromPath(fileName));
    std::string gr2FileName(getFileNameFromPath(gr2PathFile));

    if(QFile::exists(QString::fromStdString(gr2FolderFile + gr2FileName)) == false){
        //File doesn't exist handle error
        return false;
    }

    if(exporterObj->m_grannyFile != nullptr){
        GrannyFreeFile(exporterObj->m_grannyFile);
        exporterObj->m_grannyFile = nullptr;
    }
    exporterObj->resetMemory();
    exporterObj->loadedFilename = gr2FolderFile + gr2FileName;
    std::string Filename(exporterObj->loadedFilename);

    exporterObj->m_grannyFile =   exporterObj->openFile(Filename);
    exporterObj->m_fileInfo =  exporterObj->onFileOpen(exporterObj->m_grannyFile);
    if((exporterObj->m_grannyFile != nullptr)&&(exporterObj->m_fileInfo != nullptr)){
        ui->pushButton_exportCN6->setEnabled(true);
        ui->pushButton_ExportAnimNA2->setEnabled(true);
        ui->pushButton_ExportAllModelNB2->setEnabled(true);
        ui->pushButton_OpenAnimTxt->setEnabled(true);
    }
    return true;
}
bool MainWindow::readTxtFile(std::string fileNameStd){
    QString fileName = QString::fromStdString(fileNameStd);
    QFile txtFile(fileName);
    if(txtFile.open(QIODevice::ReadOnly | QIODevice::Text) == false){
        //Handle error
        return false;
    }
    std::string FolderFile(getFolderFromPath(fileName.toStdString()));

    while (!txtFile.atEnd()) {
        //Get new line
        QByteArray line = txtFile.readLine();
        //GENERAL NAME file.msa number
        //Split the line
        std::string text = line.data();
        std::istringstream iss(text);
        std::vector<std::string> textSplited(std::istream_iterator<std::string>{iss},
                                         std::istream_iterator<std::string>());
        std::string msaPath(FolderFile + textSplited.at(2));
        //Check if msa file exist
        if(QFile::exists(QString::fromStdString(msaPath)) == false){
            //File doesn't ignore the line
            continue;
        }
        //msa file found
        QFile msaFile(QString::fromStdString(msaPath));
        //Try open it
        if(msaFile.open(QIODevice::ReadOnly | QIODevice::Text) == false){
            //cannot open the msa file, ignore it
            continue;
        }
        //msa file open
        bool foundMotionFileName = false;
        std::string motionFileName;

        while (!msaFile.atEnd()) {
            //Parse file until get MotionFileName
            std::string msaText = msaFile.readLine().data();
            if(strncmp(msaText.c_str(), "MotionFileName", strlen("MotionFileName")) != 0){
                //It's not so gonext line
                continue;
            }
            //MotionFileName found

            //Skip MotionFileName
            const char * lineData = msaText.c_str();
            int indexLine = 0;
            int length = msaText.size();

            while((lineData[indexLine] != '\"')&&(indexLine < length)){
                indexLine++;
            }
            indexLine++;
            //Get content of " caracters
            while((lineData[indexLine] != '\"')&&(indexLine < length)){
                motionFileName.push_back(lineData[indexLine++]);
            }
            //Get file name
            motionFileName = getFileNameFromPath(motionFileName);
            //Add current directory
            motionFileName = FolderFile + motionFileName;

            foundMotionFileName = true;
            break;
        }
        msaFile.close();
        //Check if field motionFileName found
        if(!foundMotionFileName){
            continue;   //If not ignore the line
        }
        m_animFiles.push_back(motionFileName);
        m_animName.push_back(textSplited.at(1));
    }
    //Close the txt file
    txtFile.close();

    while(m_animFiles.size() != 0){
        //For all animation file found
        //Reset memory
        if(exporterObj->m_animFileInfo != nullptr){
            GrannyFreeFile(exporterObj->m_grannyAnimFile);
            exporterObj->m_animFileInfo = nullptr;
        }
        exporterObj->resetMemory();

        //Initiate value of exporter
        exporterObj->loadedAnimFile = m_animFiles.back();
        exporterObj->animName = m_animName.back();
        m_animFiles.pop_back();
        m_animName.pop_back();

        //Open file with granny dll
        exporterObj->m_grannyAnimFile = exporterObj->openFile(exporterObj->loadedAnimFile);
        exporterObj->m_animFileInfo =  exporterObj->onAnimFileOpen(exporterObj->m_grannyAnimFile);
        if (exporterObj->m_animFileInfo == nullptr) {
            //Cannot open files handle error
            continue;
        }
        else{
            //If successfully open
            exporterObj->exportNA2Anim(exporterObj->m_animFileInfo, 0, exporterObj->m_fileInfo);
        }
    }
    return true;
}

void MainWindow::on_pushButton_OpenMSM_clicked(){
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", tr("msm file (*.msm *.MSM)"));
    if(fileName == ""){
        return;
    }
    readMsmFile(fileName.toStdString());
}

void MainWindow::on_pushButton_OpenAnimTxt_clicked(){
    m_animFiles.clear();
    m_animName.clear();
    //Dialog
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", tr("text file (*.txt)"));
    if(fileName == ""){
        return;
    }
    readTxtFile(fileName.toStdString());
}

void MainWindow::on_pushButton_OpenFolder_clicked(){
    //Open directory
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::DirectoryOnly );

    QString dirName = dialog.getExistingDirectory(this);
    if(dirName == ""){
        return;
    }
    m_listCn6File.clear();
    m_listNa2File.clear();
    //Vector msm et txt files
    std::vector<std::string> msmFiles;
    std::vector<std::string> txtFiles;

    //Creat Dir object
    QDir directory(dirName);
    //Read *.msm file in the directory
    QStringList msmFile = directory.entryList(QStringList() << "*.msm" << "*.MSM", QDir::Files);
    foreach(QString filename, msmFile) {
        //Add the file to the vector
        msmFiles.push_back(dirName.toStdString() + std::string("\\") + filename.toStdString());
    }
    //Check if the vector isn't empty
    if(msmFiles.size() > 0){
        //Process msmFile, multi msm file unsupported for now
        readMsmFile(msmFiles.at(0));
        exporterObj->exportAllModelsToCN6(exporterObj->m_fileInfo, false);
    }
    //Read .tct file in the directory
    QStringList txtFile = directory.entryList(QStringList() << "*.txt" << "*.TXT", QDir::Files);
    foreach(QString filename, txtFile) {
        //Check that the name of file is motlist.txt
        if(filename != "motlist.txt"){
            continue;
        }
        txtFiles.push_back(dirName.toStdString() + std::string("\\") + filename.toStdString());
    }
    //Check the vector isn't empty
    if(txtFiles.size() > 0){
        readTxtFile(txtFiles.at(0));
    }
    if(m_listCn6File.size() <= 0){
        return;
    }
    std::stringstream commandShell("");
    QString currDir = QDir::currentPath();
    std::string blenderAbsolutePath(currDir.toStdString());
    blenderAbsolutePath += "\\blender-2.79b-windows64\\";

    commandShell << blenderAbsolutePath << "blender.exe ";  //Path to blender executable
    commandShell << "--background -P ";                     //Parameter to run blender in background
    commandShell << blenderAbsolutePath << "ExeImporter.py";//Link to script that import na2, cn6 in .blend
    commandShell << " -- ";     // Marque start of py arg

    //First arg is output file name, shall contain .blend
    commandShell << "out.blend ";
    //Second arg is the working directory
    commandShell << dirName.toStdString() << " ";
    //third arg is cn6 file
    commandShell << getFileNameFromPath(m_listCn6File.at(0)) << " ";
    //other parameter are na2 file
    for(unsigned int indexNa2Files = 0; indexNa2Files < m_listNa2File.size(); indexNa2Files++){
        commandShell << getFileNameFromPath(m_listNa2File.at(indexNa2Files)) << " ";
    }
    QProcess blenderProc;
    //caré intempestif
    //blender-2.79b-windows64\blender.exe --background -P blender-2.79b-windows64\testExeImporter.py -- res.blend C:\Travail\Merlo\Jeu\stray_dog\ stray_dog.cn6 37_1__0.000-0.800.na2
    blenderProc.start(commandShell.str().c_str());

    while(!blenderProc.waitForFinished()){
        /*QByteArray line = blenderProc.readLine();
        if(line.size() > 0){
            qDebug() << line << endl;
        }*/
    }
    std::string resultCmd = blenderProc.readAll().toStdString();
    //clean the generated file
    for(unsigned int indexCn6Files = 0; indexCn6Files < m_listCn6File.size(); indexCn6Files++){
        QFile file2remove(QString::fromStdString(m_listCn6File.at(indexCn6Files)));
    }
    for(unsigned int indexNa2Files = 0; indexNa2Files < m_listNa2File.size(); indexNa2Files++){
        QFile file2remove(QString::fromStdString(m_listNa2File.at(0)));
    }
}
