#ifndef GRANNYMESHVERTEXSTRUCTINFO_H
#define GRANNYMESHVERTEXSTRUCTINFO_H

#include <string>
#include <unistd.h>
#include "granny.h"
#include "NumberUtils.h"

class GrannyMeshVertexStructInfo
{
    public:
        GrannyMeshVertexStructInfo(int itype, std::string iname, int icount);
        virtual ~GrannyMeshVertexStructInfo();

		int type;
		int count;
		std::string name;
        int length;
        int offset;

        int getLength();
        void * convert(void * pointer);
        void * convertBack(void * number);
    protected:

    private:
};

#endif // GRANNYMESHVERTEXSTRUCTINFO_H
