#ifndef GRANNYSKELETONWRAPPER_H
#define GRANNYSKELETONWRAPPER_H

#include <unistd.h>
#include <cstring>
#include "granny.h"
#include "GrannyBoneWrapper.h"
#include "GrannySkeletonInfo.h"
#include "GrannyBoneInfo.h"

class GrannySkeletonWrapper
{
    public:
        GrannySkeletonWrapper(granny_skeleton * inputSkeleton);
        virtual ~GrannySkeletonWrapper();

        granny_skeleton * wrappedSkeleton;
        granny_skeleton * m_pkSkeleton;

        void setName(std::string name);
        int getNumBones();
        void setNumBones(int num);
        granny_bone * getBonesPtr();
        GrannySkeletonInfo readSkeletonInfo();
        void writeSkeletonInfo(GrannySkeletonInfo skeletonInfo);

    protected:

    private:
        int boneSize = 152;
};
/*

*/
#endif // GRANNYSKELETONWRAPPER_H
