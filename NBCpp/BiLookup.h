#ifndef BILOOKUP_H
#define BILOOKUP_H

#include <vector>
#include <map>

template <class TFirst, class TSecond>
class BiLookup
{
    public:
        BiLookup();
        virtual ~BiLookup();

        std::map<TFirst, std::vector<TSecond>> firstToSecond;
        std::map<TSecond, std::vector<TFirst>> secondToFirst;

        std::vector<TFirst> EmptyFirstList;
        std::vector<TSecond> EmptySecondList;

        void Add(TFirst first, TSecond second);

        std::vector<TSecond> operator [](TFirst first);
        std::vector<TFirst> operator [](TSecond second);

        std::vector<TSecond> GetByFirst(TFirst first);
        std::vector<TFirst> GetBySecond(TSecond second);
    protected:

    private:

};

#endif // BILOOKUP_H
