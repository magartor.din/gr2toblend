#ifndef GRANNYTRANSFORMWRAPPER_H
#define GRANNYTRANSFORMWRAPPER_H

#include <unistd.h>
#include "granny.h"

class GrannyTransformWrapper
{
    public:
        GrannyTransformWrapper(granny_transform * inputTransform);
        virtual ~GrannyTransformWrapper();

        granny_transform wrappedTransform;
        granny_transform * m_pkTransform;

    protected:

    private:
};
/*
IntPtr getTransformPtr()
virtual unsafe float[] ScaleShear
virtual unsafe float[] Orientation
virtual unsafe float[] Position
virtual unsafe int Flags

*/
#endif // GRANNYTRANSFORMWRAPPER_H
