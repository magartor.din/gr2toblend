#include "GrannyBoneWrapper.h"

extern int newIndicator;

GrannyBoneWrapper::GrannyBoneWrapper(granny_bone * inputBone)
{
    //ctor
    this->wrappedBone = inputBone;
}

GrannyBoneWrapper::~GrannyBoneWrapper()
{
    //dtor
}
std::string GrannyBoneWrapper::getName(){
    std::string res(this->wrappedBone->Name);
    return res;
}
void GrannyBoneWrapper::setName(std::string name){
    this->wrappedBone->Name = name.c_str();
}

float * GrannyBoneWrapper::getPosition(){
    float * matrixValue = new float[3];
    newIndicator++;
    for (int j = 0; j < 3; j++){
        matrixValue[j] = this->wrappedBone->LocalTransform.Position[j];
    }
    return matrixValue;
   // return (float *)this->wrappedBone->LocalTransform.Position;
}
float * GrannyBoneWrapper::getOrientation(){
    float * matrixValue = new float[4];
    newIndicator++;
    for (int j = 0; j < 4; j++){
        matrixValue[j] = this->wrappedBone->LocalTransform.Orientation[j];
    }
    return matrixValue;
    //return (float *)this->wrappedBone->LocalTransform.Orientation;
}
void GrannyBoneWrapper::setScaleShear(float * scaleShear){
    for (int j = 0; j < 9; j++){
        ((float *)this->wrappedBone->LocalTransform.ScaleShear)[j] = scaleShear[j];
    }
}
float ** GrannyBoneWrapper::getScaleShear(){
    float ** matrixValue = new float*[3];
    newIndicator++;
    for(int i = 0; i < 3; i++){
        matrixValue[i] = new float[3];
        newIndicator++;
    }

    for (int j = 0; j < 3; j++){
        for (int i = 0; i < 3; i++){
            matrixValue[j][i] = (float)((this->wrappedBone->LocalTransform.ScaleShear[j])[i]);
        }
    }
    return matrixValue;
    //return (float *)this->wrappedBone->LocalTransform.ScaleShear;
}
float ** GrannyBoneWrapper::getInverseWorldMatrixPtr(){
    return (float **)this->wrappedBone->InverseWorld4x4;
}
float ** GrannyBoneWrapper::getInverseWorldMatrix(){
    float ** matrixValue = new float *[4];
    newIndicator++;
    for(int i = 0; i < 4; i++){
        matrixValue[i] = new float[4];
        newIndicator++;
    }
    for (int j = 0; j < 4; j++){
        for (int i = 0; i < 4; i++){
            matrixValue[j][i] = (float)((this->wrappedBone->InverseWorld4x4[j])[i]);
        }
    }
    return matrixValue;
}

