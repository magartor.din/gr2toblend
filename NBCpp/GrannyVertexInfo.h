#ifndef GRANNYVERTEXINFO_H
#define GRANNYVERTEXINFO_H

#include <unistd.h>
#include "granny.h"

class GrannyVertexInfo
{
    public:
        GrannyVertexInfo();
        virtual ~GrannyVertexInfo();
        float * position; //[3]
        int * boneWeights; //[4]
        int * boneIndices; //[4]
        float * normal; //[3]
        float * uv; //[2]
        float * tangent; //[3]
        float * binormal; //[3]
    protected:

    private:
};

#endif // GRANNYVERTEXINFO_H
