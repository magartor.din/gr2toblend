#include "GrannyMaterialWrapper.h"

GrannyMaterialWrapper::GrannyMaterialWrapper(granny_material * inputMaterial)
{
    this->wrappedMaterial = inputMaterial;
    this->m_pkMaterial = inputMaterial;
    //ctor
}

GrannyMaterialWrapper::~GrannyMaterialWrapper()
{
    //dtor
}

std::string GrannyMaterialWrapper::getName(){
    return this->wrappedMaterial->Name;
}
void GrannyMaterialWrapper::setName(std::string name){
    this->wrappedMaterial->Name = name.c_str();
}

granny_variant * GrannyMaterialWrapper::getExtendedDataPtr(){
    return &(this->wrappedMaterial->ExtendedData);
}
std::string GrannyMaterialWrapper::getShaderSetName(){
    //Dans ce cas l'object repr�sente un pointeur vers une chaine de caract�re
    if(this->wrappedMaterial->ExtendedData.Type->Type == GrannyStringMember){
        std::string strOut((char *)*(int*)this->wrappedMaterial->ExtendedData.Object);
        return strOut;
    }
    else{
        std::string strOut("");
        return strOut;
    }
}
