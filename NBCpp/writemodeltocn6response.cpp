#include "writemodeltocn6response.h"

WriteModelToCN6Response::WriteModelToCN6Response(std::vector<std::string> decalMeshNames, std::vector<std::string> textureMaps){
    this->decalMeshNames = decalMeshNames;
    this->textureMaps = textureMaps;
}
