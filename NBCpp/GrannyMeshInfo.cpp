#include "GrannyMeshInfo.h"

GrannyMeshInfo::GrannyMeshInfo()
{
    //ctor
}

GrannyMeshInfo::~GrannyMeshInfo()
{
    //dtor
}
void GrannyMeshInfo::setVertexStructInfos(std::vector<GrannyMeshVertexStructInfo> inVertexStructInfos)
{
    this->vertexStructInfos = inVertexStructInfos;
    int currentOffset = 0;
    int i = 0;
    for (GrannyMeshVertexStructInfo structInfo : this->vertexStructInfos)
    {
        this->vertexStructInfos[i].offset = currentOffset;
        currentOffset += this->vertexStructInfos[i].length * this->vertexStructInfos[i].count;
        i++;
    }
    bytesPerVertex = currentOffset;
}

GrannyMeshVertexStructInfo * GrannyMeshInfo::getVertexStructInfoByName(std::string name) {
    int i = 0;
    for (GrannyMeshVertexStructInfo structInfo : this->vertexStructInfos) {
        if (structInfo.name.compare(name) == 0)
        {
            return &(vertexStructInfos[i]);
        }
        i++;
    }
    return nullptr;
}
