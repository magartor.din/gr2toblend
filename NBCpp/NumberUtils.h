#ifndef NUMBERUTILS_H
#define NUMBERUTILS_H

#include <unistd.h>
#include "granny.h"
#include <string>
#include <cstring>
#include <math.h>

#define N 4

class NumberUtils
{
    public:
        NumberUtils();
        virtual ~NumberUtils();

        static float halfToFloat(unsigned short u);
        // http://stackoverflow.com/questions/6162651/half-precision-floating-point-in-java/6162687#6162687
        static unsigned short floatToHalf(float f);
        static unsigned short floatToHalfOld(float f);
        static int parseInt(std::string str);
        static float parseFloat(std::string str);
        static bool almostEquals(float float1, float float2, int precision);
        template <typename T>
        static T MatrixDet(T **Mat, int n);
    protected:

    private:

};
/*



*/
#endif // NUMBERUTILS_H
