#ifndef GRANNYMODELINFO_H
#define GRANNYMODELINFO_H

#include <unistd.h>
#include <vector>
#include "GrannyMeshInfo.h"
#include "GrannySkeletonInfo.h"
#include "granny.h"

class GrannyModelInfo
{
    public:
        GrannyModelInfo();

        GrannySkeletonInfo skeleton;
        std::vector<GrannyMeshInfo> meshBindings;
    protected:

    private:
};

#endif // GRANNYMODELINFO_H
