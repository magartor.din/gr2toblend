#ifndef GRANNYMESHINFO_H
#define GRANNYMESHINFO_H

#include <unistd.h>
#include <vector>
#include <string>

#include "granny.h"
#include "GrannyVertexInfo.h"
#include "GrannyMeshVertexStructInfo.h"

class GrannyMeshInfo
{
    public:
        GrannyMeshInfo();
        virtual ~GrannyMeshInfo();

        std::string name;
        std::vector<GrannyVertexInfo> vertices;
        std::vector<int *> triangles;
        std::vector<std::string> boneBindings;
        std::string materialName;

        std::vector<GrannyMeshVertexStructInfo> vertexStructInfos;
        int bytesPerVertex;

        void setVertexStructInfos(std::vector<GrannyMeshVertexStructInfo> inVertexStructInfos);
        GrannyMeshVertexStructInfo * getVertexStructInfoByName(std::string name);
    protected:

    private:
};

#endif // GRANNYMESHINFO_H
