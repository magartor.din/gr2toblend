#include "GrannyMeshVertexStructInfo.h"
extern int newIndicator;

GrannyMeshVertexStructInfo::GrannyMeshVertexStructInfo(int itype, std::string iname, int icount)
{
    //ctor
    this->type = itype;
    this->name = iname;
    this->count = icount;
    this->length = this->getLength();
}

GrannyMeshVertexStructInfo::~GrannyMeshVertexStructInfo()
{
    //dtor
}
int GrannyMeshVertexStructInfo::getLength() {
    switch(this->type) {
        case 10: return 4;  // 32bit float
        case 11: return 1;  // granny_int8
        case 12: return 1;  // granny_uint8
        case 14: return 1;  // granny_uint8
        case 21: return 2;  // 16bit float
        default: return 0;
    }
}
void * GrannyMeshVertexStructInfo::convert(void * pointer){
    switch (this->type)
    {
        case 10:{
            newIndicator++;
            float * f = new float;
            *f = *(float*)pointer;
            return (void *)f;
            }
        //case 11: return 1;  // granny_int8
        case 12:{
            newIndicator++;
            unsigned char * b = new char unsigned;
            *b = *(unsigned char *)pointer;
            return (void *)b;  // granny_uint8
            }
        case 14:{
            newIndicator++;
            unsigned char * b = new char unsigned;
            *b = *(unsigned char *)pointer;
            return (void *)b;  // granny_uint8
            }
        case 21:{
            newIndicator++;
            float * f = new float;
            *f = NumberUtils::halfToFloat(*(unsigned short*)pointer);
            return f;  // 16bit float
            }
        default: return nullptr;
    }
}
void* GrannyMeshVertexStructInfo::convertBack(void * number){
    switch (this->type)
    {
        case 10:{
            newIndicator++;
            float * f = new float;
            *f = *(float *)number;
            return f;
            }
        //case 11: return 1;  // granny_int8
        case 12:{
            newIndicator++;
            char * b = new char;
            *b = *(char *)number;
            return b; // granny_uint8
            }
        case 14:{
            newIndicator++;
            char * b = new char;
            *b = *(char *)number;
            return b; // granny_uint8
            }
        case 21:{
           // UInt16 half = NumberUtils.floatToHalf((float)number);
           // return &half; // 16bit float
            return nullptr;
            }
        default: return nullptr;
    }
}
