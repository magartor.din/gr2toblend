#ifndef GRANNYMATERIALWRAPPER_H
#define GRANNYMATERIALWRAPPER_H

#include <unistd.h>
#include <string>
#include <cstring>
#include "granny.h"

class GrannyMaterialWrapper
{
    public:
        GrannyMaterialWrapper(granny_material * inputMaterial);
        virtual ~GrannyMaterialWrapper();

        granny_material * wrappedMaterial;
        granny_material * m_pkMaterial;

        std::string getName();
        void setName(std::string name);

        granny_variant * getExtendedDataPtr();
        std::string getShaderSetName();
    protected:

    private:
};
/*


*/
#endif // GRANNYMATERIALWRAPPER_H
