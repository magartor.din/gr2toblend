#ifndef WRITEMODELTOCN6RESPONSE_H
#define WRITEMODELTOCN6RESPONSE_H

#include <unistd.h>
#include "granny.h"
#include <string>
#include <cstring>
#include <math.h>
#include <vector>

using namespace std;

class WriteModelToCN6Response
{
public:
    WriteModelToCN6Response(std::vector<std::string> decalMeshNames, std::vector<std::string> textureMaps);

    std::vector<std::string> decalMeshNames;
    std::vector<std::string> textureMaps;
};

#endif // WRITEMODELTOCN6RESPONSE_H
