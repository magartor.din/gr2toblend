#include "IndieAnimDef.h"

IndieAnimDef::IndieAnimDef()
{
    //ctor
}

IndieAnimDef::~IndieAnimDef()
{
    //dtor
}

std::string IndieAnimDef::getEventCodes(void){
    std::string text;
    for (unsigned int i = 0; i < this->eventCodes.size(); i++){
        text += this->eventCodes[i];
        if (i < this->eventCodes.size() - 1){
            text += ", ";
        }
    }
    return text;
}
void IndieAnimDef::setEventCodes(std::string value){
    if(value.empty()){
        return;
    }
    this->eventCodes.clear();
    char buffTempo[1024] = {0};
    strcpy(buffTempo, value.c_str());
    char * token = strtok(buffTempo, ", ");

    // loop through the string to extract all other tokens
    while( token != NULL ) {
        this->eventCodes.push_back(atoi(token));
        token = strtok(NULL, ", ");
    }
}
