#ifndef GRANNYBONEINFO_H
#define GRANNYBONEINFO_H

#include <unistd.h>
#include <string>

#include "GrannyTransformInfo.h"
#include "granny.h"

class GrannyBoneInfo
{
    public:
        GrannyBoneInfo();

        std::string name;
        GrannyTransformInfo localTransform;
        float **inverseWorldTransform;
        float LODError;
        int parentIndex;
    protected:

    private:
};

#endif // GRANNYBONEINFO_H
