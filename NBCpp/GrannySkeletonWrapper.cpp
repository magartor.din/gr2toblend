#include "GrannySkeletonWrapper.h"

GrannySkeletonWrapper::GrannySkeletonWrapper(granny_skeleton * inputSkeleton)
{
    this->m_pkSkeleton = inputSkeleton;
    this->wrappedSkeleton = inputSkeleton;
}

GrannySkeletonWrapper::~GrannySkeletonWrapper()
{

}

void GrannySkeletonWrapper::setName(std::string name){
    this->wrappedSkeleton->Name = name.c_str();
}
int GrannySkeletonWrapper::getNumBones(){
    return this->wrappedSkeleton->BoneCount;
}
void GrannySkeletonWrapper::setNumBones(int num){
    this->wrappedSkeleton->BoneCount = num;
}
granny_bone * GrannySkeletonWrapper::getBonesPtr(){
    return this->wrappedSkeleton->Bones;
}
GrannySkeletonInfo GrannySkeletonWrapper::readSkeletonInfo(){
    GrannySkeletonInfo skeletonInfo;
    std::vector<GrannyBoneInfo> boneInfos;
    for(int bi = 0; bi < this->wrappedSkeleton->BoneCount; bi++){
        granny_bone * bone = &this->wrappedSkeleton->Bones[bi];
        GrannyBoneInfo boneInfo;
        GrannyTransformInfo boneTransformInfo;
        granny_transform boneTransform = bone->LocalTransform;

        boneInfo.name = bone->Name;
        boneInfo.parentIndex = bone->ParentIndex;
        boneTransformInfo.flags = GrannyTransformInfo::getFlagsInt(boneTransform.Flags);

        GrannyBoneWrapper boneWrapper(bone);
        boneTransformInfo.orientation = boneWrapper.getOrientation();
        boneTransformInfo.position = boneWrapper.getPosition();
        boneTransformInfo.scaleShear = boneWrapper.getScaleShear();

        boneInfo.localTransform = boneTransformInfo;

        boneInfo.inverseWorldTransform = (float **)bone->InverseWorld4x4;
        boneInfo.LODError = bone->LODError;

        boneInfos.push_back(boneInfo);
    }
    skeletonInfo.bones = boneInfos;
    return skeletonInfo;
}
void GrannySkeletonWrapper::writeSkeletonInfo(GrannySkeletonInfo skeletonInfo){
    int bonesCount = skeletonInfo.bones.size();
    this->setNumBones(bonesCount);

    granny_bone * oldBonesPtr = this->getBonesPtr();
    this->wrappedSkeleton->Bones = (granny_bone *)malloc(bonesCount * sizeof(granny_bone));
    granny_bone * newBonesPtr = this->getBonesPtr();

    std::vector<GrannyBoneInfo> boneInfos(skeletonInfo.bones.begin(),skeletonInfo.bones.end());
    for (int i = 0; i < bonesCount; i++){
        memcpy(&newBonesPtr[i], oldBonesPtr, sizeof(granny_bone));

        GrannyBoneInfo currentBone = boneInfos[i];

        newBonesPtr[i].Name = currentBone.name.c_str();
        newBonesPtr[i].ParentIndex = currentBone.parentIndex;

        int flags = currentBone.localTransform.flags;

        newBonesPtr[i].LocalTransform.Flags = flags;

        newBonesPtr[i].LocalTransform.Position[0] = currentBone.localTransform.position[0];
        newBonesPtr[i].LocalTransform.Position[1] = currentBone.localTransform.position[1];
        newBonesPtr[i].LocalTransform.Position[2] = currentBone.localTransform.position[2];

        newBonesPtr[i].LocalTransform.Orientation[0] = currentBone.localTransform.orientation[0];
        newBonesPtr[i].LocalTransform.Orientation[1] = currentBone.localTransform.orientation[1];
        newBonesPtr[i].LocalTransform.Orientation[2] = currentBone.localTransform.orientation[2];
        newBonesPtr[i].LocalTransform.Orientation[3] = currentBone.localTransform.orientation[3];

        for (int j = 0; j < 3; j++){
            for(int k = 0; k < 3; k++){
                newBonesPtr[i].LocalTransform.ScaleShear[j][k] = currentBone.localTransform.scaleShear[j][k];
            }
        }
        for (int j = 0; j < 4; j++){
            for (int k = 0; k < 4; k++){
                newBonesPtr[i].InverseWorld4x4[j][k] = currentBone.inverseWorldTransform[j][k];
            }
        }
        newBonesPtr[i].LODError = 1.0f;
    }
}
