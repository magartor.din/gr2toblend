#include "IndieMaterial.h"


IndieMaterial::IndieMaterial(granny_material * material)
{
    this->material = material;
}

const char * IndieMaterial::GetType(){
    if(this->material != nullptr){
        if(this->material->ExtendedData.Type != nullptr){
            if(strcmp(this->material->ExtendedData.Type->Name, "ShaderSet") == 0){
                if(this->material->ExtendedData.Type->Type == GrannyStringMember){
                    char * str = (char *)(*(int*)this->material->ExtendedData.Object);
                    if(str == nullptr){
                        return "None";
                    }
                    else{
                        std::string shaderSet(str);
                        if (shaderSet == "SimpleShader")
                        {
                            return "IndieSimpleShader";
                        }
                        if ((shaderSet == "FXShader") || (shaderSet == "FXShader_Skinned"))
                        {
                            return "IndieFXShader";
                        }
                        if ((shaderSet == "BuildingShader") || (shaderSet == "BuildingShader_Skinned"))
                        {
                            return "IndieBuildingShader";
                        }
                        if (shaderSet == "Leader")
                        {
                            return "IndieLeaderShader";
                        }
                        if (shaderSet == "Leader_Opaque_Cloth")
                        {
                            return "IndieLeaderOpaqueClothShader";
                        }
                        if (shaderSet == "Leader_Skin")
                        {
                            return "IndieLeaderSkinShader";
                        }
                        if (shaderSet == "Leader_Fur")
                        {
                            return "IndieLeaderFurShader";
                        }
                        if (shaderSet == "Leader_Transparency")
                        {
                            return "IndieLeaderTransparencyShader";
                        }
                        if (shaderSet == "Leader_Hair")
                        {
                            return "IndieLeaderHairShader";
                        }
                        if (shaderSet == "Leader_Opaque_Hair")
                        {
                            return "IndieLeaderOpaqueHairShader";
                        }
                        if (shaderSet == "Leader_Glass")
                        {
                            return "IndieLeaderGlassShader";
                        }
                        if (shaderSet == "Leader_Fur_Fin")
                        {
                            return "IndieLeaderFurFinShader";
                        }
                        if (shaderSet == "Leader_Velvet")
                        {
                            return "IndieLeaderVelvetShader";
                        }
                        if (shaderSet == "Leader_Masked")
                        {
                            return "IndieLeaderMaskedShader";
                        }
                        if (shaderSet == "Leader_Masked_Hair")
                        {
                            return "IndieLeaderMaskedHairShader";
                        }
                        if (shaderSet == "Leader_Opaque_Matte")
                        {
                            return "IndieLeaderOpaqueMatteShader";
                        }
                        if (shaderSet == "Leader_Transparent_Matte")
                        {
                            return "IndieLeaderTransparentMatteShader";
                        }
                        if ((shaderSet == "UnitShader_Skinned") || (shaderSet == "UnitShader") || (shaderSet == "UnitShader_SkinnedAlpha"))
                        {
                            return "IndieUnitSkinnedShader";
                        }
                        if (shaderSet == "LandmarkShader_Stencil")
                        {
                            return "IndieLandmarkStencilShader";
                        }
                        if (shaderSet != "LandmarkShader")
                        {

                        }
                        return "IndieLandmarkStencilShader";
                    }
                }
                else{

                }
                if(this->material->ExtendedData.Object != nullptr){

                }
            }
        }
    }
    return "None";
}
granny_material * IndieMaterial::getMaterial(){
    return this->material;
}
IndieMaterial::~IndieMaterial(){

}
void IndieMaterial::CopyTextureIfExists(std::string file, std::string outputFolder){
    if(file.empty()){
        return;
    }
    std::string fileRes;
    for(unsigned int i = 0; i < file.size(); i++){
        if((file.at(i) == ' ')||
            (file.at(i) == '\n')){
                continue;
            }
        fileRes += file.at(i);
    }
    if(fileRes.empty()){
        return;
    }

}
void IndieMaterial::setName(std::string name){
    this->m_name = name;
}
std::string IndieMaterial::trimPathFromFileName(std::string str){
    int i;
    if(str.size()==0)
        return std::string("");
    for(i = str.size()-1; (str.at(i) != '\\')&&(str.at(i) != '/')&&(i>0); i--);
    return std::string(str.substr(i+1, str.size()-i));
}
std::string IndieMaterial::getFileNameByUsageName(std::string usageName){
    granny_material_map * currMatMap = nullptr;
    for(int i = 0; i < this->material->MapCount; i++){
        if(strcmp(this->material->Maps[i].Usage, usageName.c_str()) == 0){
            currMatMap = &(this->material->Maps[i]);
            break;
        }
    }
    if(currMatMap == nullptr){
        //Material map not found
        return std::string("");
    }
    //Material found
    if(strcmp(currMatMap->Material->ExtendedData.Type->Name, "File Name") == 0){
        //Check extend data is file name
        if(currMatMap->Material->ExtendedData.Type->Type == GrannyStringMember){
            //Check extend data is a string
            return trimPathFromFileName(std::string((char *)*(int *)currMatMap->Material->ExtendedData.Object));
        }
    }
    if(currMatMap->Material->Texture != nullptr){
        //May be texture contain file name
        if(currMatMap->Material->Texture->FromFileName != nullptr){
            //File name isn't nullptr
            return trimPathFromFileName(std::string(currMatMap->Material->Texture->FromFileName));
        }
    }
    //No file name found
    return std::string("");
}
std::string IndieMaterial::getBuildingSREF(void){
    return getFileNameByUsageName(std::string("BuildingSREF"));
}
std::string IndieMaterial::getDiffuse(void){
    return getFileNameByUsageName(std::string("Diffuse"));
}
std::string IndieMaterial::getBaseTextureMap(void){
    return getFileNameByUsageName(std::string("BaseTextureMap"));
}
std::string IndieMaterial::getSREFMap(void){
    return getFileNameByUsageName(std::string("SREFMap"));
}
std::string IndieMaterial::getSpecTextureMap(void){
    return getFileNameByUsageName(std::string("SpecTextureMap"));
}
std::string IndieMaterial::getIrradiance(void){
    return getFileNameByUsageName(std::string("Irradiance"));
}
std::string IndieMaterial::getSpecSharp(void){
    return getFileNameByUsageName(std::string("SpecSharp"));
}
std::string IndieMaterial::getSpecSoft(void){
    return getFileNameByUsageName(std::string("SpecSoft"));
}
std::string IndieMaterial::getDistanceFogRamp(void){
    return getFileNameByUsageName(std::string("DistanceFogRamp"));
}
std::string IndieMaterial::getColorCorrection(void){
    return getFileNameByUsageName(std::string("ColorCorrection"));
}
std::string IndieMaterial::getFOWColorKey(void){
    return getFileNameByUsageName(std::string("FOWColorKey"));
}
std::string IndieMaterial::getFOWMask(void){
    return getFileNameByUsageName(std::string("FOWMask"));
}
std::string IndieMaterial::getFOWBaseLayer(void){
    return getFileNameByUsageName(std::string("FOWBaseLayer"));
}
std::string IndieMaterial::getUVScrollingMap0(void){
    return getFileNameByUsageName(std::string("UVScrollingMap0"));
}
std::string IndieMaterial::getAlphaLookupMap0(void){
    return getFileNameByUsageName(std::string("AlphaLookupMap0"));
}
std::string IndieMaterial::getHeightTextureMap(void){
    return getFileNameByUsageName(std::string("HeightTextureMap"));
}
std::string IndieMaterial::getFUR_BaseMap(void){
    return getFileNameByUsageName(std::string("Fur_BaseMap"));
}
std::string IndieMaterial::getFUR_SREF(void){
    return getFileNameByUsageName(std::string("Fur_SREF"));
}
std::string IndieMaterial::getFUR_Transparency(void){
    return getFileNameByUsageName(std::string("Fur_Transparency"));
}
std::string IndieMaterial::getFUR_IrradianceMap(void){
    return getFileNameByUsageName(std::string("Fur_IrradianceMap"));
}
std::string IndieMaterial::getNormalMap(void){
    return getFileNameByUsageName(std::string("NormalMap"));
}
std::string IndieMaterial::getIrradianceMap(void){
    return getFileNameByUsageName(std::string("IrradianceMap"));
}
std::string IndieMaterial::getEnvironmentMap(void){
    return getFileNameByUsageName(std::string("EnvironmentMap"));
}
std::string IndieMaterial::getDiffuseMap(void){
    return getFileNameByUsageName(std::string("DiffuseMap"));
}
std::string IndieMaterial::getTransparencyMap(void){
    return getFileNameByUsageName(std::string("Transparency"));
}
std::string IndieMaterial::getTangentMap(void){
    return getFileNameByUsageName(std::string("TangentMap"));
}
std::string IndieMaterial::getMaskMap(void){
    return getFileNameByUsageName(std::string("Mask"));
}
std::string IndieMaterial::getMatte(void){
    return getFileNameByUsageName(std::string("Matte"));
}
std::string IndieMaterial::getSkinBlurMap(void){
    return getFileNameByUsageName(std::string("SkinBlurMap"));
}
std::string IndieMaterial::getTransparency(void){
    return getFileNameByUsageName(std::string("Transparency"));
}
std::string IndieMaterial::getBaseMap(void){
    return getFileNameByUsageName(std::string("VelvetBaseMap"));
}
std::string IndieMaterial::getSheenMap(void){
    return getFileNameByUsageName(std::string("VelvetSheenMap"));
}
std::string IndieMaterial::getSPECMap(void){
    return getFileNameByUsageName(std::string("SpecMap"));
}
std::string IndieMaterial::getBaseSampler(void){
    return getFileNameByUsageName(std::string("BaseSampler"));
}
std::string IndieMaterial::getEnvironmentMaskSampler(void){
    return getFileNameByUsageName(std::string("EnvironmentMaskSampler"));
}
std::string IndieMaterial::getLightCubeMapSampler(void){
    return getFileNameByUsageName(std::string("LightCubeMapSampler"));
}
std::string IndieMaterial::getEnvironmentMapSampler(void){
    return getFileNameByUsageName(std::string("EnvironmentMapSampler"));
}
std::string IndieMaterial::getTeamColorSampler(void){
    return getFileNameByUsageName(std::string("TeamColorSampler"));
}
std::string IndieMaterial::getIrradianceCubeMap(void){
    return getFileNameByUsageName(std::string("IrradianceCubeMap"));
}
std::string IndieMaterial::getDullEnvironmentCubeMap(void){
    return getFileNameByUsageName(std::string("DullEnvironmentCubeMap"));
}
std::string IndieMaterial::getEnvironmentCubeMap(void){
    return getFileNameByUsageName(std::string("EnvironmentCubeMap"));
}
