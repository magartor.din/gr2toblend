#include "fileconfig.h"

#include <algorithm>
#include <cctype>

FileConfig::FileConfig()
{
    folderNameBlender = "\\blender-2.79b-windows64\\";

    grannyFileExtension = "gr2 file (*.gr2 *.GR2)";
    msmFileExtension = "msm file (*.msm *.MSM)";
    txtFileExtension = "text file (*.txt)";
    nameFieldModelMsm = "BaseModelFileName";
    nameFieldAnimTxt = "MotionFileName";
    blenderExeName = "blender.exe";
    blenderParameter = "--background -P";
    convScript = "ExeImporter.py";
    outputFileName = "out.blend";
}

std::string FileConfig::getExtension(std::string extName){
    std::string res;
    "gr2 file (*.gr2 *.GR2)"

    std::transform(extName.begin(), extName.end(), extName.begin(),
        [](unsigned char c){ return std::tolower(c); });

    std::transform(extName.begin(), extName.end(), extName.begin(),
        [](unsigned char c){ return std::toupper(c); });

    res
}
void FileConfig::resetConfig(void){

}
bool FileConfig::loadConfig(std::string filePath){

}
bool FileConfig::saveConfig(std::string filePath){

}
bool FileConfig::updateConfig(std::string filePath){

}
