#ifndef EXPORTER_H
#define EXPORTER_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <map>
#include <vector>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <limits>
#include <QObject>
#include <cstdio>
#include <algorithm>

#include "NBCpp/granny.h"
#include "NBCpp/GrannyMeshWrapper.h"
#include "NBCpp/BiLookup.h"
#include "NBCpp/Matrice.h"
#include "NBCpp/IndieMaterial.h"
#include "NBCpp/IndieAnimDef.h"
#include "NBCpp/GrannyMaterialWrapper.h"
#include "NBCpp/writemodeltocn6response.h"

using namespace std;

class Exporter : public QObject
{
    Q_OBJECT
public:
    Exporter();
    static void findAndReplaceAll(std::string & data, std::string toSearch, std::string replaceStr);
    static double ** getBoneWorldMatrix(granny_bone * bone);
    static double * getBoneWorldPosition(granny_bone * bone);

    void exportNB2AllModel(granny_file_info* fileInfo);
    std::string GetNewAnimName(void);
    void addAnimation(granny_file_info * fileInfo);
    granny_file * openFile(std::string FileName);
    granny_file_info * onFileOpen(granny_file * grannyFile);
    granny_file_info * onAnimFileOpen(granny_file * grannyAnimFile);
    int getBoneIdForBoneName(BiLookup<int, std::string> boneLookup, std::map<std::string, double *> boneNameToPositionMap, std::string boneName, float boneWeight, float * vertexPosition);;
    void exportNB2Model(granny_file_info* fileInfo, int modelId);
    unsigned int sampleModel(granny_file_info* fileInfo, int animId, granny_file_info * modelFileInfo, std::map<std::string,std::vector<float *>> * res);
    void exportNA2Anim(granny_file_info* fileInfo, int animId, granny_file_info * modelFileInfo);
    void AddAnimToListbox(granny_animation * grannyAnimation);
    void AddMaterialToListbox(granny_material * material);
    void AddMeshToListbox(granny_mesh * mesh);
    void refresh(granny_file_info * fileInfo);
    std::vector<std::string> exportAllModelsToCN6(granny_file_info * fileInfo, bool isBatch);
    std::vector<string> cn6Export(granny_file_info *  grannyFile, int currentModelIndex, bool isBatch);
    WriteModelToCN6Response WriteModelToCN6(granny_file_info * grannyFile, bool isBatch, granny_model * model, std::string * filterMeshName);
    std::string GetSafeFilename(std::string filename);
    void resetMemory();

    std::vector<IndieAnimDef> animDefList;
    std::vector<granny_material *> materialList;
    std::vector<granny_mesh *> meshList;
    std::vector<granny_animation *> animList;
    std::vector<std::string> usedShaderSet;
    std::vector<std::string> bonesComboBox;
    int bonesComboBoxIndex = 0;
    std::vector<granny_model *> modelList;
    int currentModelIndex = -1;
    std::string loadedStringDatabaseFilename;
    std::string loadedFilename;
    std::string loadedAnimFile;

    granny_file * m_grannyFile;
    granny_file_info * m_fileInfo;
    granny_file * m_grannyAnimFile;
    granny_file_info * m_animFileInfo;
    std::string animName;
signals:
    void refreshData(granny_file_info * fileInfo);
    void createNa2File(std::string name);
    void createCn6File(std::string name);
private:

};

#endif // EXPORTER_H
