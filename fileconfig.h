#ifndef FILECONFIG_H
#define FILECONFIG_H

#include <QObject>
#include <QFile>
#include <string>

class FileConfig
{
public:
    FileConfig();

    void resetConfig(void);
    bool loadConfig(std::string filePath);
    bool saveConfig(std::string filePath);
    bool updateConfig(std::string filePath);
    std::string getExtension(std::string extName);
private:
    bool configFileIsOpen;
    QFile configFile;
    std::string folderNameBlender;
    std::string txtFileExtension;
    std::string grannyFileExtension;
    std::string msmFileExtension;
    std::string nameFieldModelMsm;
    std::string nameFieldAnimTxt;
    std::string blenderExeName;
    std::string blenderParameter;
    std::string convScript;
    std::string outputFileName;
};

#endif // FILECONFIG_H
