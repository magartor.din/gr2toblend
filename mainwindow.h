#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "exporter.h"
#include "fileconfig.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_Open_clicked();

    void on_pushButton_ExportAllModelNB2_clicked();

    void on_pushButton_ExportAnimNA2_clicked();

    void on_pushButton_exportCN6_clicked();

    void on_pushButton_OpenMSM_clicked();

    void on_pushButton_OpenAnimTxt_clicked();

    void on_pushButton_OpenFolder_clicked();
    void on_ExporterNewNa2File(std::string fileName);
    void on_ExporterNewCn6File(std::string fileName);
private:
    std::string getFileNameFromPath(std::string str);
    std::string getFolderFromPath(std::string str);

    bool readMsmFile(std::string fileName);
    bool readTxtFile(std::string fileNameStd);
    Ui::MainWindow *ui;
    Exporter * exporterObj;
    FileConfig configObj;
    std::vector<std::string> m_animFiles;
    std::vector<std::string> m_animName;

    std::vector<std::string> m_listNa2File;
    std::vector<std::string> m_listCn6File;
};
#endif // MAINWINDOW_H
