#include "exporter.h"
#include "granny.h"

extern int newIndicator;

Exporter::Exporter(){
    m_grannyFile = nullptr;
    m_fileInfo = nullptr;
    m_grannyAnimFile = nullptr;
    m_animFileInfo = nullptr;

}
void Exporter::findAndReplaceAll(std::string & data, std::string toSearch, std::string replaceStr){
    // Get the first occurrence
    size_t pos = data.find(toSearch);

    // Repeat till end is reached
    while( pos != std::string::npos)
    {
        // Replace this occurrence of Sub String
        data.replace(pos, toSearch.size(), replaceStr);
        // Get the next occurrence from the current position
        pos =data.find(toSearch, pos + replaceStr.size());
    }
}
double ** Exporter::getBoneWorldMatrix(granny_bone * bone){
    double ** res;
    double ** src;

    newIndicator++;
    src = new double *[4];
    for(int i = 0; i < 4; i++){
        newIndicator++;
        src[i] = new double[4];
        src[i][0] = (double)bone->InverseWorld4x4[i][0];
        src[i][1] = (double)bone->InverseWorld4x4[i][1];
        src[i][2] = (double)bone->InverseWorld4x4[i][2];
        src[i][3] = (double) bone->InverseWorld4x4[i][3];
    }

    Matrice<double> matInverser(src, 4);
    res = matInverser.MatrixInverse();
    for(int i = 0; i < 4; i++){
        delete src[i];
    }
    delete src;
    return res;
}
double * Exporter::getBoneWorldPosition(granny_bone * bone){
    double ** worldMatrix = getBoneWorldMatrix(bone);

    newIndicator++;
    double * bonePosition = new double[3];
    bonePosition[0] = worldMatrix[3][0];
    bonePosition[1] = worldMatrix[3][1];
    bonePosition[2] = worldMatrix[3][2];

    for(int i = 0; i < 4; i++){
        delete worldMatrix[i];
    }
    delete worldMatrix;

    return bonePosition;
}
void Exporter::exportNB2AllModel(granny_file_info* fileInfo){
    for(int i = 0; i < fileInfo->ModelCount; i++){
        exportNB2Model(fileInfo, i);
    }
}
std::string Exporter::GetNewAnimName(void){
    std::string text;
    int num = animDefList.size() + 1;
    text = "NewAnimation";
    text += num;
    return text;
}
void Exporter::addAnimation(granny_file_info * fileInfo){
    if(fileInfo == nullptr){
        return;
    }
    IndieAnimDef indieAnimDef;
    indieAnimDef.Name = GetNewAnimName();
    indieAnimDef.EndFrame = "0";
    if(fileInfo->AnimationCount > 0){
        indieAnimDef.EndFrame = ceil((double)(fileInfo->Animations[0]->Duration)/(2*fileInfo->Animations[0]->TimeStep));
    }
    indieAnimDef.StartFrame = "0";
    indieAnimDef.EventCodes = "";
    if(animDefList.size() == 0){
        indieAnimDef.EventCodes = "1000, 1020, 1040, 1100, 1120, 1140, 1160, 1180, 1200, 1220, 1280, 1285, 1290, 1400, 1440, 1450, 1500, 1520, 1540, 1560, 1580, 1600, 1620, 1640, 1800, 2000, 2020, 2040, 2100, 2200, 2220, 2440";
    }
    animDefList.push_back(indieAnimDef);
    return;
}
granny_file * Exporter::openFile(std::string FileName){
    granny_file* grannyFile =  GrannyReadEntireFile(FileName.c_str());

    if (grannyFile == nullptr) {
        printf("Could not load %s\n", FileName.c_str());
        return nullptr;
    }
    return grannyFile;

}
granny_file_info * Exporter::onFileOpen(granny_file * grannyFile){
    granny_file_info * fileInfo =  GrannyGetFileInfo(grannyFile);
    refresh(fileInfo);
    emit refreshData(fileInfo);  //Testé en mode debug apparament même résultats
    addAnimation(fileInfo);
    return fileInfo;
}
granny_file_info * Exporter::onAnimFileOpen(granny_file * grannyAnimFile){
    granny_file_info * animFileInfo =  GrannyGetFileInfo(grannyAnimFile);
    return animFileInfo;
}
int Exporter::getBoneIdForBoneName(BiLookup<int, std::string> boneLookup, std::map<std::string, double *> boneNameToPositionMap, std::string boneName, float boneWeight, float * vertexPosition){
    int boneId;
    if ((boneName == "")/*||(true)*/){
        if (boneWeight == 0){
            boneId = 1;
        }
        else{
            double minDistance = std::numeric_limits<double>::max();
            std::string closestBoneName;
            std::map<std::string, double *>::iterator it = boneNameToPositionMap.begin();
            while (it != boneNameToPositionMap.end()) {
                double * bonePosition = boneNameToPositionMap[it->first];
                double deltaX = vertexPosition[0] - bonePosition[0];
                double deltaY = vertexPosition[1] - bonePosition[1];
                double deltaZ = vertexPosition[2] - bonePosition[2];
                double distance = sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);

                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestBoneName = it->first;
                }
                it++;
            }
            if(!closestBoneName.empty()){
                //std::string tempo = boneLookup[closestBoneName];
                boneId = boneLookup[closestBoneName].at(0);
            }
            else{
                boneId = 1;
            }

        }
    }
    else{
        std::vector<int> resTempo = boneLookup[boneName];
        boneId = resTempo.at(0);
    }
    return boneId;
}

void Exporter::exportNB2Model(granny_file_info* fileInfo, int modelId){
    std::string fileExtension(".nb2");
    std::string outputFilename;
    if (fileInfo->ModelCount > 1){
        std::string modelFilename("__");
        modelFilename += fileInfo->Models[modelId]->Name;
        modelFilename += fileExtension;

        outputFilename += loadedFilename;
        findAndReplaceAll(outputFilename, ".gr2", modelFilename);
        findAndReplaceAll(outputFilename, ".GR2", modelFilename);
    }
    else{
        outputFilename += loadedFilename;
        findAndReplaceAll(outputFilename, ".gr2", fileExtension);
        findAndReplaceAll(outputFilename, ".GR2", fileExtension);
    }
    ofstream myfile;
    myfile.open(outputFilename, ios_base::out);
    if(!myfile.is_open()){
         std::cout << "File opening is fail." << endl;
         return;
    }

    granny_model * model = fileInfo->Models[modelId];
    granny_skeleton * skeleton = model->Skeleton;
    std::map<int, int> meshBindingToMesh;
    std::vector<string> distinctMeshNames;
    for (int i = 0; i < model->MeshBindingCount; i++){
        for (int j = 0; j < fileInfo->MeshCount; j++){
            GrannyMeshWrapper modelMesh(model->MeshBindings[i].Mesh);
            granny_mesh * fileMesh = fileInfo->Meshes[j];
            if(modelMesh.meshEqual(fileMesh)){
                meshBindingToMesh.insert(std::pair<int, int>(i ,j));
            }
        }
        distinctMeshNames.push_back(model->MeshBindings[i].Mesh->Name);
    }
    std::map<std::string, int> meshNameCount;
    for(std::string meshName : distinctMeshNames){
        meshNameCount.insert(std::pair<string, int>(meshName, 0));
    }
    std::vector<GrannyMeshInfo> grannyMeshInfos;
    for(int i = 0; i < model->MeshBindingCount; i++){
        GrannyMeshWrapper meshWrapper(model->MeshBindings[i].Mesh);
        grannyMeshInfos.push_back(meshWrapper.getMeshInfo());
    }
    BiLookup<int, std::string> boneLookup;
    for(int i = 0; i < skeleton->BoneCount; i++){
        std::string boneName(skeleton->Bones[i].Name);
        boneLookup.Add(i, boneName);
    }
    std::map<std::string, double *> boneNameToPositionMap;
    double * bonePosition;
    for(int i = 0; i < skeleton->BoneCount; i ++){
        granny_bone bone = skeleton->Bones[i];
        try{
            bonePosition = getBoneWorldPosition(&bone);
            std::string boneName(bone.Name);
            boneNameToPositionMap.insert(std::pair<std::string, double *>(boneName, bonePosition));
        }catch(...){
            throw;
        }
    }
    myfile << "// Nexus Buddy NB2 - Exported from Nexus Buddy 2" << endl;
    myfile << "Frames: 30" << endl;
    myfile << "Frame: 1" << endl;
    std::vector<granny_material *> modelMaterials;
    for(int i = 0; i < model->MeshBindingCount; i++){
        granny_mesh * mesh = model->MeshBindings[i].Mesh;
        granny_material * material = mesh->MaterialBindings[0].Material;

        for(unsigned int mlii = 0; mlii < materialList.size(); mlii++){
            granny_material * materialFromList = materialList[mlii];

            IndieMaterial shader(materialFromList);
            GrannyMaterialWrapper matWrap1(shader.getMaterial());
            GrannyMaterialWrapper matWrap2(material);

            if (matWrap1.getName() == matWrap2.getName()){
                bool contain = false;
                for(unsigned int vmm = 0; vmm < modelMaterials.size(); vmm++){
                    if(modelMaterials[vmm] == materialFromList){
                        contain = true;
                        break;
                    }
                }
                if(contain == false){
                    modelMaterials.push_back(materialFromList);
                }
            }
        }

    }
    myfile << "Meshes: " << model->MeshBindingCount << endl;
    for (unsigned int mi = 0; mi < grannyMeshInfos.size(); mi++){
        GrannyMeshInfo grannyMeshInfo = grannyMeshInfos[mi];
        std::string meshName = model->MeshBindings[mi].Mesh->Name;
        meshNameCount[meshName]++;
        if(meshNameCount[meshName] > 1){
            meshName += meshNameCount[meshName];
        }
        const char * materialBindingName = model->MeshBindings[mi].Mesh->MaterialBindings[0].Material->Name;
        int materialIndex = 0;
        for (unsigned int mmi = 0; mmi < modelMaterials.size(); mmi++){
            granny_material * materialIndi = modelMaterials[mmi];
            if (strcmp(materialBindingName, materialIndi->Name) == 0){
                break;
            }
            materialIndex++;
        }

        myfile << "\"" << meshName << "\" 0 " << materialIndex << endl;
        myfile << grannyMeshInfo.vertices.size() << endl;
        for (unsigned int vi = 0; vi < grannyMeshInfo.vertices.size(); vi++){
            GrannyVertexInfo vertex = grannyMeshInfo.vertices[vi];

            std::string boneName0 = grannyMeshInfo.boneBindings[vertex.boneIndices[0]];
            float boneWeight0 = (float)vertex.boneWeights[0] / 255;
            int boneId0 = getBoneIdForBoneName(boneLookup, boneNameToPositionMap, boneName0, boneWeight0, vertex.position);

            std::string boneName1 = grannyMeshInfo.boneBindings[vertex.boneIndices[1]];
            float boneWeight1 = (float)vertex.boneWeights[1] / 255;
            int boneId1 = getBoneIdForBoneName(boneLookup, boneNameToPositionMap, boneName1, boneWeight1, vertex.position);

            std::string boneName2 = grannyMeshInfo.boneBindings[vertex.boneIndices[2]];
            float boneWeight2 = (float)vertex.boneWeights[2] / 255;
            int boneId2 = getBoneIdForBoneName(boneLookup, boneNameToPositionMap, boneName2, boneWeight2, vertex.position);

            std::string boneName3 = grannyMeshInfo.boneBindings[vertex.boneIndices[3]];
            float boneWeight3 = (float)vertex.boneWeights[3] / 255;
            int boneId3 = getBoneIdForBoneName(boneLookup, boneNameToPositionMap, boneName3, boneWeight3, vertex.position);

            //char strTempo[256] = {0};


            myfile << std::fixed << std::setprecision(6) << "0 " << vertex.position[0] << " " << vertex.position[1] << " " << vertex.position[2] << " "
                   << vertex.uv[0] << " " << vertex.uv[1] << " "
                   << boneId0 << " " << boneWeight0 << " " << boneId1 << " " << boneWeight1 << " "
                   << boneId2 << " " << boneWeight2 << " " << boneId3 << " " << boneWeight3 << endl;
        }
        // Write Normals
        myfile << grannyMeshInfo.vertices.size() << endl;
        for(unsigned int ni = 0; ni < grannyMeshInfo.vertices.size(); ni++){
            GrannyVertexInfo vertex = grannyMeshInfo.vertices[ni];
            myfile << vertex.normal[0] << " " << vertex.normal[1] << " " << vertex.normal[2] << endl;
        }
        // Write Triangles
        myfile << grannyMeshInfo.triangles.size() << endl;
        for (unsigned int ti = 0; ti < grannyMeshInfo.triangles.size(); ti++){
            int * triangle = grannyMeshInfo.triangles[ti];
            myfile << "0 " << triangle[0] << " " << triangle[1] << " " << triangle[2] << " " << triangle[0] << " " << triangle[1] << " " << triangle[2] << " 1" << endl;
        }
    }
    // Write Materials
    myfile << "Materials: " << modelMaterials.size() << endl;
    for (unsigned int i = 0; i < modelMaterials.size(); i++ ){
        myfile << "\"" << modelMaterials[i]->Name << "\"" << endl;
        myfile << "0.200000 0.200000 0.200000 1.000000" << endl;
        myfile << "0.800000 0.800000 0.800000 1.000000" << endl;
        myfile << "0.000000 0.000000 0.000000 1.000000" << endl;
        myfile << "0.000000 0.000000 0.000000 1.000000" << endl;
        myfile << "0.000000" << endl;
        myfile << "1.000000" << endl;

        std::string map1("ColorMap_");
        map1 += i;
        std::string map2("AlphaMap_");
        map2 += i;
        std::string baseMap;
        std::string srefmap;
        GrannyMaterialWrapper modelMaterialWrapper(modelMaterials[i]);
        if (strcmp(modelMaterialWrapper.getShaderSetName().c_str(), "BuildingShader") == 0){
            baseMap = "Diffuse";
            srefmap = "BuildingSREF";
        }
        else if(strcmp(modelMaterialWrapper.getShaderSetName().c_str(), "LandmarkStencilShader") == 0){
            baseMap = "BaseTextureMap";
            srefmap = "SpecTextureMap";
        }
        else{
            baseMap = "BaseTextureMap";
            srefmap = "SREFMap";
        }

        if (!baseMap.empty()){
            map1 = baseMap;
        }

        if (!srefmap.empty()){
            map2 = srefmap;
        }
        myfile << "\"" << map1 << "\"" << endl;
        myfile << "\"" << map2 << "\"" << endl;
    }
    // Write Bones
    myfile << "Bones: " << skeleton->BoneCount << endl;
    for (int bi = 0; bi < skeleton->BoneCount; bi++){
        granny_bone bone = skeleton->Bones[bi];
        std::string boneName(bone.Name);
        myfile << "\"" << boneName << "\"" << endl;
        if (bone.ParentIndex == -1){
            myfile << "\"\"" << endl;
        }
        else{
            std::string parentBoneName(skeleton->Bones[bone.ParentIndex].Name);
            myfile << "\"" << parentBoneName << "\"" << endl;
        }
        granny_transform transforme = bone.LocalTransform;
        float * orientation = transforme.Orientation;
        float * position = transforme.Position;
        myfile << "0 " << position[0] << " " << position[1] << " " << position[2] << " "
               << orientation[0] << " " << orientation[1] << " " << orientation[2] << " " << orientation[3] << endl;
        // number of position keys
        myfile << "0" << endl;
        // number of rotation keys
        myfile << "0" << endl;
    }
    myfile.close();
}

unsigned int Exporter::sampleModel(granny_file_info* fileInfo, int animId, granny_file_info * modelFileInfo, std::map<std::string,std::vector<float *>> * res){
    int nbFrame = 0;
    //Map that represent the annimation
    res->clear();
    //Create all the bones in the map
    for(int indexBone = 0; indexBone < modelFileInfo->Skeletons[0]->BoneCount; indexBone++){
        //Vector that represent a frame
        std::vector<float *> Frame;
        //Bone name
        std::string boneName(modelFileInfo->Skeletons[0]->Bones[indexBone].Name);
        //Add bone frame to map
        res->insert(std::pair<std::string,std::vector<float *>>(boneName, Frame));
    }
    //Get model
    granny_model *Model = modelFileInfo->Models[0];
    //Instanciate model
    granny_model_instance *InstanceOfModel = GrannyInstantiateModel(Model);
    //Get bone count
    granny_int32x BoneCount = GrannyGetSourceSkeleton(InstanceOfModel)->BoneCount;
    //Create local pose
    granny_local_pose *LocalPose = GrannyNewLocalPose(BoneCount);
    //Create world pose
    granny_world_pose *WorldPose = GrannyNewWorldPose(BoneCount);
    //Get skeleton
    granny_skeleton * Skeleton = modelFileInfo->Skeletons[0];
    //Model initial place (ident matrix)
    granny_matrix_4x4 ModelTransform = {{1,0,0,0},
                                        {0,1,0,0},
                                        {0,0,1,0},
                                        {0,0,0,1}
                                        };
    //Create control matrix
    granny_control *Control = GrannyPlayControlledAnimation(0, fileInfo->Animations[animId], InstanceOfModel);
    //Total animation time
    //granny_real32 TotalTime = GrannyGetControlDuration(Control);
    //Time left
    granny_real32 TimeLeft = GrannyGetControlDurationLeft(Control);
    //Current time
    granny_real32 ControlClock = GrannyGetControlClock(Control);
    //inc Time
    granny_real32 incTime = 1.0f/60;
    // Set current time to 0
    GrannySetControlClock(Control, 0);

    while(TimeLeft >= 0){   //While animation isn't finish
        nbFrame ++;
        //Sample for control
        GrannySampleSingleModelAnimation(InstanceOfModel, Control, 0, BoneCount, LocalPose);
        //Calcul world pose
        GrannyBuildWorldPose(Skeleton, 0, Skeleton->BoneCount, LocalPose, (granny_real32 *)ModelTransform, WorldPose);
        //Get 4x4matrix array from world pose
        granny_matrix_4x4 *WorldTransformArray = GrannyGetWorldPose4x4Array(WorldPose);
        //Get float array for each bone
        for(int indexBone = 0; indexBone < modelFileInfo->Skeletons[0]->BoneCount; indexBone++){
            int idBone = 0;
            std::string boneName(modelFileInfo->Skeletons[0]->Bones[indexBone].Name);
            GrannyFindBoneByName(modelFileInfo->Skeletons[0], boneName.c_str(), &idBone);
            granny_real32 *BoneWorld = (granny_real32 *)WorldTransformArray[idBone];
            newIndicator++;
            float * table = new float[16];
            memcpy(table, BoneWorld, 16*sizeof(float));
            //res[boneName].push_back(table);
            res->at(boneName).push_back(table);
        }
        //Get current time
        ControlClock = GrannyGetControlClock(Control);
        //Inc current time
        GrannySetControlClock(Control, ControlClock+incTime);
        //Calculate time left
        TimeLeft = GrannyGetControlDurationLeft(Control);
    }
    //Free control
    GrannyFreeControl(Control);
    //Free local pose
    GrannyFreeLocalPose(LocalPose);
    //Free world pose
    GrannyFreeWorldPose(WorldPose);
    //Free instance model
    GrannyFreeModelInstance(InstanceOfModel);

    return nbFrame;
}
void Exporter::exportNA2Anim(granny_file_info* fileInfo, int animId, granny_file_info * modelFileInfo){

    granny_animation * anim = fileInfo->Animations[animId];
    std::string filename(loadedAnimFile);
    int startFrame = -1;
    int endFrame = -1;
    int frameCount = 0;
    int fps = round(1.0f/anim->TimeStep);
    fps = 60;
    float timeWindowStart = 0;
    //float timestep = 1.0f/fps;

    std::vector<std::string> boneNameList;
    for(int i = 0; i < anim->TrackGroups[0]->TransformTrackCount; i++){
        boneNameList.push_back( anim->TrackGroups[0]->Name);
    }
//    int retryCount = 0;
//    bool success = false;

    std::map<std::string,std::vector<float *>> boneFrameLists;
    //Fill the array with tha values
    endFrame = sampleModel(fileInfo, animId, modelFileInfo, &boneFrameLists);
    frameCount = endFrame;
    std::stringstream fileExtensionStream("");
    fileExtensionStream << "__" << std::fixed << std::setprecision(3) << timeWindowStart << "-" << anim->Duration << ".na2";
    findAndReplaceAll(filename, ".gr2", fileExtensionStream.str());
    std::string outputFile(filename);
    findAndReplaceAll(outputFile, ".GR2", fileExtensionStream.str());

    ofstream myfile;
    myfile.open(outputFile, ios_base::out);
    emit createNa2File(outputFile);

    startFrame = 0;

    myfile << "// Nexus Buddy Animation NA2 - Exported from Nexus Buddy 2" << endl;
    myfile << "FrameSets: 1" << endl;
    myfile << "FrameCount: " << frameCount << endl;
    myfile << "FirstFrame: " << startFrame << endl;
    myfile << "LastFrame: " << endFrame << endl;
    myfile << "FPS: " << fps << endl;
    myfile << "Name: "<< animName << endl;
    myfile << "Bones: " << anim->TrackGroups[0]->TransformTrackCount << endl;

    myfile << std::fixed << std::setprecision(6);
    for(int indexBone = 0; indexBone < modelFileInfo->Skeletons[0]->BoneCount; indexBone++){
        //Bone name
        std::string boneName(modelFileInfo->Skeletons[0]->Bones[indexBone].Name);
        std::vector<float *> FrameList = boneFrameLists[boneName];
        myfile << boneName << endl;
        for(unsigned int frame = 0; frame < FrameList.size(); frame++){
            float * matriceKff = FrameList.at(frame);
            for(int j = 0; j < 16; j++){
                myfile << matriceKff[j] << " ";
            }
            myfile << endl;
            delete matriceKff;
        }

    }
    myfile.close();
}

void Exporter::AddAnimToListbox(granny_animation * grannyAnimation){
    animList.push_back(grannyAnimation);
}
void Exporter::AddMaterialToListbox(granny_material * material){
    std::string shaderSet;
    try{
        GrannyMaterialWrapper materialWrapper(material);
        shaderSet = materialWrapper.getShaderSetName();
        if (shaderSet.empty()){
            usedShaderSet.push_back(shaderSet);
            materialList.push_back(material);
        }
        usedShaderSet.push_back(shaderSet);
        if (shaderSet == "SimpleShader"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "FXShader" || shaderSet == "FXShader_Skinned"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "BuildingShader" || shaderSet == "BuildingShader_Skinned"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Opaque_Cloth"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Skin"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Fur"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Transparency"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Hair"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Opaque_Hair"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Glass"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Fur_Fin"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Velvet"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Masked"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Masked_Hair"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Opaque_Matte"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "Leader_Transparent_Matte"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "UnitShader_Skinned" || shaderSet == "UnitShader" || shaderSet == "UnitShader_SkinnedAlpha"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet == "LandmarkShader_Stencil"){
            materialList.push_back(material);
            return;
        }
        if (shaderSet != "LandmarkShader"){
            return;
        }
        materialList.push_back(material);

    }
    catch (const std::exception & e ){
        std::cerr << e.what();
    }
}
void Exporter::AddMeshToListbox(granny_mesh * mesh){
    meshList.push_back(mesh);
}
void Exporter::refresh(granny_file_info * fileInfo){
    std::stringstream strFileInfo("");
    std::map<std::string, int> materialNameCount;
    if(fileInfo != nullptr){
        if(currentModelIndex == -1){
            currentModelIndex = 0;
        }
        animDefList.clear();
        materialList.clear();
        meshList.clear();
        animList.clear();
        if((fileInfo->ModelCount > 0)&&(fileInfo->Models[currentModelIndex]->Skeleton != nullptr)){
            granny_skeleton * skeleton = fileInfo->Models[currentModelIndex]->Skeleton;
            bonesComboBox.clear();
            for(int i = 0; i < skeleton->BoneCount; i++){
                std::string tempoName(skeleton->Bones[i].Name);
                bonesComboBox.push_back(tempoName);
            }
            bonesComboBoxIndex = 0;
        }
        if((int) modelList.size() != fileInfo->ModelCount){
            for(int i = 0; i < fileInfo->ModelCount; i ++){
                modelList.push_back(fileInfo->Models[i]);
            }
        }
        if(fileInfo->ModelCount > 0){
            for(int i = 0; i < fileInfo->Models[currentModelIndex]->MeshBindingCount; i++){
                AddMeshToListbox(fileInfo->Models[currentModelIndex]->MeshBindings[i].Mesh);
            }
        }
        for(int i = 0; i < fileInfo->MaterialCount; i++){
            granny_material * currentMaterial =  fileInfo->Materials[i];
            int materialIndex = 0;
            GrannyMaterialWrapper materialWrapper(currentMaterial);
            std::string currentMaterialName = materialWrapper.getName();
            if(currentMaterialName == ""){
                currentMaterialName = "Material";
                currentMaterialName += materialIndex;
            }
            if(materialNameCount.find(currentMaterialName) == materialNameCount.end()){
                materialNameCount.insert(std::pair<std::string, int>(currentMaterialName, 1));
            }
            else{
                materialNameCount[currentMaterialName] = materialNameCount[currentMaterialName] + 1;
                std::string newName;
                newName += currentMaterialName;
                newName += "_";
                newName += materialNameCount[currentMaterialName];

                materialWrapper.setName(newName);
            }
            AddMaterialToListbox(currentMaterial);
            materialIndex++;
        }
        for(int i = 0; i < fileInfo->AnimationCount; i++){
            granny_animation * currentAnimation = fileInfo->Animations[i];
            AddAnimToListbox(currentAnimation);
        }
    }
}
std::vector<std::string> Exporter::exportAllModelsToCN6(granny_file_info *fileInfo, bool isBatch){
    std::vector<std::string> fileTextureMaps;

    for (int i = 0; i < fileInfo->ModelCount; i++){
         std::vector<std::string> textureMaps = cn6Export(fileInfo, i, isBatch);
        fileTextureMaps.insert( fileTextureMaps.end(), textureMaps.begin(), textureMaps.end() );
    }

    return fileTextureMaps;
}
std::vector<string> Exporter::cn6Export(granny_file_info *  grannyFile, int currentModelIndex, bool isBatch){
    granny_model * model = grannyFile->Models[currentModelIndex];
    WriteModelToCN6Response response = WriteModelToCN6(grannyFile, isBatch, model, nullptr);
    for(unsigned int i = 0; i < response.decalMeshNames.size(); i++){
        std::string decalMeshName(response.decalMeshNames[i]);
        WriteModelToCN6(grannyFile, isBatch, model, &decalMeshName);
    }
    return response.textureMaps;
}

std::string Exporter::GetSafeFilename(std::string filename){
    std::string res(filename);
    findAndReplaceAll(res, "\n", "_");
    findAndReplaceAll(res, "\r", "_");

    findAndReplaceAll(res, "<", "_");
    findAndReplaceAll(res, ">", "_");

    findAndReplaceAll(res, ":", "_");
    findAndReplaceAll(res, "\"", "_");

    findAndReplaceAll(res, "/", "_");
    findAndReplaceAll(res, "\\", "_");

    findAndReplaceAll(res, "|", "_");
    findAndReplaceAll(res, "?", "_");
    findAndReplaceAll(res, "!", "_");
    findAndReplaceAll(res, "*", "_");
    findAndReplaceAll(res, "%", "_");
    findAndReplaceAll(res, ".", "_");
    findAndReplaceAll(res, " ", "_");

    return res;
}

WriteModelToCN6Response Exporter::WriteModelToCN6(granny_file_info * grannyFile, bool isBatch, granny_model * model, std::string * filterMeshName){
    std::vector<std::string> textureMaps;
    std::vector<std::string> decalMeshNames;
    granny_skeleton * skeleton = model->Skeleton;

    std::string fileExtension = ".cn6";
    std::string outputFilename = "";
    std::string decalMeshName = "";

    if (filterMeshName != nullptr){
        decalMeshName = "_DCL_" + GetSafeFilename(*filterMeshName);
    }

    if (grannyFile->ModelCount > 1){
        std::string modelFilename("__");
        modelFilename += model->Name;
        modelFilename += fileExtension;

        outputFilename += loadedFilename;
        findAndReplaceAll(outputFilename, ".gr2", modelFilename);
        findAndReplaceAll(outputFilename, ".GR2", modelFilename);
    }
    else{
        outputFilename += loadedFilename;
        findAndReplaceAll(outputFilename, ".gr2", fileExtension);
        findAndReplaceAll(outputFilename, ".GR2", fileExtension);
    }

    if (isBatch){
       findAndReplaceAll(outputFilename, fileExtension, "_batch" + fileExtension);
    }
    /*
    std::string directory = Path.GetDirectoryName(outputFilename);
    std::string filename = Path.GetFileName(outputFilename);
    outputFilename = directory + "\\" + GetSafeFilename(filename);
    */
    ofstream myfile;
    emit createCn6File(outputFilename);
    myfile.open(outputFilename, ios_base::out);

    // Lookup so we can identify the meshes belonging the current model in the list of file meshes
    std::map<int, int> meshBindingToMesh;
    std::vector<std::string> distinctMeshNames;
    for (int i = 0; i < model->MeshBindingCount; i++){
        for (int j = 0; j < grannyFile->MeshCount; j++){
            GrannyMeshWrapper modelMesh(model->MeshBindings[i].Mesh);
            granny_mesh * fileMesh = grannyFile->Meshes[j];
            if (modelMesh.meshEqual(fileMesh)){
                meshBindingToMesh.insert(std::pair<int, int>(i,j));
            }
        }
        distinctMeshNames.push_back(model->MeshBindings[i].Mesh->Name);
    }
    // Used to give meshes distinct names where we have multiple meshes with the same name in our source gr2
    std::map<std::string, int> meshNameCount;
    for(unsigned int i = 0; i < distinctMeshNames.size(); i++){
        std::string meshName(distinctMeshNames.at(i));
        meshNameCount.insert(std::pair<std::string, int>(meshName, 0));
    }

    std::vector<GrannyMeshInfo> grannyMeshInfos;
    for (int i = 0; i < model->MeshBindingCount; i++){
        GrannyMeshWrapper meshWrapper(model->MeshBindings[i].Mesh);
        grannyMeshInfos.push_back(meshWrapper.getMeshInfo());
    }

    BiLookup<int, string> boneLookup;
    for (int i = 0; i < skeleton->BoneCount; i++){
        boneLookup.Add(i, skeleton->Bones[i].Name);
    }

    std::map<std::string, double *> boneNameToPositionMap;
    for(int i = 0; i < skeleton->BoneCount; i++){
        granny_bone * bone = &(skeleton->Bones[i]);
        if (!(boneNameToPositionMap.count(bone->Name) > 0)){
            double * bonePosition = getBoneWorldPosition(bone);
            std::string boneNameTempo(bone->Name);
            boneNameToPositionMap.insert(std::pair<std::string, double *>(boneNameTempo, bonePosition));
        }
    }

    myfile << "// CivNexus6 CN6 - Exported from Nexus Buddy" << endl;
    myfile << "skeleton" << endl;

    // Write Bones
    for (int boneIndex = 0; boneIndex < skeleton->BoneCount; boneIndex++){
        granny_bone * bone = &(skeleton->Bones[boneIndex]);
        string boneName = bone->Name;
        granny_transform * transform = &(bone->LocalTransform);

        float * orientation = transform->Orientation;
        float * position = transform->Position;
        //float * invWorldTransform = (float *)bone->InverseWorld4x4;

        std::stringstream boneStringBuilder;
        boneStringBuilder << std::fixed << std::setprecision(8);

        boneStringBuilder << boneIndex << " \"" << boneName << "\" " << bone->ParentIndex << " ";
        boneStringBuilder << position[0] << " " << position[1] << " " << position[2] << " ";
        boneStringBuilder << orientation[0] << " " << orientation[1] << " " << orientation[2] << " " << orientation[3];

        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j ++){
                boneStringBuilder << " " << bone->InverseWorld4x4[i][j];
            }
        }
        myfile << boneStringBuilder.str() << endl;
    }

    std::vector<granny_material *> modelMaterials;
    for (int i = 0; i < model->MeshBindingCount; i++){
        granny_mesh * mesh = model->MeshBindings[i].Mesh;
        granny_material * material = mesh->MaterialBindings[0].Material;
        for(unsigned int mlii = 0; mlii < materialList.size(); mlii++){
            granny_material * materialFromList = materialList[mlii];

            IndieMaterial shader(materialFromList);
            GrannyMaterialWrapper matWrap1(shader.getMaterial());
            GrannyMaterialWrapper matWrap2(material);

            if (matWrap1.getName() == matWrap2.getName()){
                bool contain = false;
                for(unsigned int vmm = 0; vmm < modelMaterials.size(); vmm++){
                    if(modelMaterials[vmm] == materialFromList){
                        contain = true;
                        break;
                    }
                }
                if(contain == false){
                    modelMaterials.push_back(materialFromList);
                }
            }
        }
    }


    int meshCount = 0;
    for (unsigned int mi = 0; mi < grannyMeshInfos.size(); mi++){
        granny_mesh * mesh = model->MeshBindings[mi].Mesh;
        std::string meshName = model->MeshBindings[mi].Mesh->Name;

        GrannyMeshWrapper meshWrapper(mesh);
        GrannyMeshInfo meshInfo = meshWrapper.getMeshInfo();

        bool isFlatMesh = true;

        for(unsigned int vmi = 0; vmi < meshInfo.vertices.size(); vmi++){
            GrannyVertexInfo vertexInfo = meshInfo.vertices.at(vmi);
            float zPos = vertexInfo.position[2];
            bool isZero = NumberUtils::almostEquals(zPos, 0.0f, 4);
            if (!isZero){
                isFlatMesh = false;
                break;
            }
        }

        if (filterMeshName == nullptr){
            if (isFlatMesh){
                decalMeshNames.push_back(meshName);
            }
            else{
                meshCount++;
            }
        }
        else{
            meshCount = 1;
        }
    }

    if (meshCount == 0){
        myfile.close();
        std::remove(outputFilename.c_str());
        return WriteModelToCN6Response(decalMeshNames, textureMaps);
    }
    // Write Meshes
    myfile << "meshes:" << meshCount << endl;
    for (unsigned int mi = 0; mi < grannyMeshInfos.size(); mi++){
        GrannyMeshInfo grannyMeshInfo = grannyMeshInfos[mi];
        std::string meshName = model->MeshBindings[mi].Mesh->Name;

        if (((filterMeshName == nullptr) && (std::find (decalMeshNames.begin(), decalMeshNames.end(), meshName) == decalMeshNames.end())) || meshName == *filterMeshName)
        {
            meshNameCount[meshName]++;
            if (meshNameCount[meshName] > 1)
            {
                meshName += meshNameCount[meshName];
            }
            myfile << "mesh:\"" << meshName << "\"" << endl;

            // Write Materials
            myfile << "materials" << endl;
            for (int igm = 0; igm < model->MeshBindings[mi].Mesh->MaterialBindingCount; igm++){
                granny_material * material = model->MeshBindings[mi].Mesh->MaterialBindings[igm].Material;

                if(material->Name != nullptr){
                    if(strcmp(material->ExtendedData.Type->Name, "ShaderSet") == 0){
                        //Material is Civ format
                        IndieMaterial shader(material);
                        std::string baseMap;

                        if (strcmp(shader.GetType(), "IndieBuildingShader") == 0){
                            baseMap = shader.getDiffuse();

                        }
                        else if (strcmp(shader.GetType(), "IndieLandmarkStencilShader") == 0){
                            baseMap = shader.getBaseTextureMap();

                        }
                        else{
                            baseMap = shader.getBaseTextureMap();
                        }

                        if (!baseMap.empty()){
                            myfile << "\"" << baseMap << "\"" << endl;
                            textureMaps.push_back(baseMap);
                        }
                        else{
                            myfile << "\"" << material->Name << "\"" << endl;
                        }
                    }
                    else if(strcmp(material->ExtendedData.Type->Name, "Shader Type") == 0){
                        //Material is Metin2 format
                        bool diffMaterial = false;
                        for(int i = 0; i < material->MapCount; i++){
                            for(int j = i+1; j < material->MapCount; j++){
                                if(material->Maps[i].Material != material->Maps[j].Material){
                                    diffMaterial = true;
                                }
                            }
                        }
                        if(diffMaterial == true){
                            //There is ambiguity
                            myfile << "\"" << IndieMaterial::trimPathFromFileName(material->Maps[0].Material->Texture->FromFileName) << "\"" << endl;

                        }
                        else{
                            //Structure as stray dog, no ambiguity
                            //Check if there is texture
                            if(material->MapCount > 0){
                                if(material->Maps[0].Material->Texture != nullptr){
                                    myfile << "\"" << IndieMaterial::trimPathFromFileName(material->Maps[0].Material->Texture->FromFileName) << "\"" << endl;
                                }
                                else{
                                    myfile << "\"" << material->Name << "\"" << endl;
                                }
                            }
                            else{
                                //There isn't map
                                myfile << "\"" << material->Name << "\"" << endl;
                            }
                        }
                    }
                    else{
                        //not civ5 nor metin 2 need to be implemented
                        myfile << "\"" << material->Name << "\"" << endl;
                    }
                }

            }

            // Write Vertices
            myfile << "vertices" << endl;
            for (unsigned int vi = 0; vi < grannyMeshInfo.vertices.size(); vi++){
                GrannyVertexInfo vertex = grannyMeshInfo.vertices[vi];

                newIndicator++;
                std::string * boneNames = new std::string[8];
                float * boneWeights = new float[8];
                int * boneIds = new int[8];

                for (int z = 0; z < 8; z++){
                    if (z > 3) {
                        //boneNames[z] = grannyMeshInfo.boneBindings[vertex.boneIndices[z]];
                        boneWeights[z] = 0.0f;
                        boneIds[z] = 0;
                    }
                    else{
                        boneNames[z] = grannyMeshInfo.boneBindings[vertex.boneIndices[z]];
                        boneWeights[z] = (float)vertex.boneWeights[z] / 255;
                        boneIds[z] = getBoneIdForBoneName(boneLookup, boneNameToPositionMap, boneNames[z], boneWeights[z], vertex.position);
                    }
                }
                delete[] boneNames;
                delete[] boneWeights;

                newIndicator++;
                float * tangents = new float[3];
                if (vertex.tangent == nullptr)
                {
                    tangents[0] = vertex.normal[0];
                    tangents[1] = vertex.normal[1];
                    tangents[2] = vertex.normal[2];
                }
                else
                {
                    tangents[0] = vertex.tangent[0];
                    tangents[1] = vertex.tangent[1];
                    tangents[2] = vertex.tangent[2];
                }

                newIndicator++;
                float * binormals = new float[3];
                if (vertex.binormal == nullptr)
                {
                    binormals[0] = vertex.normal[0];
                    binormals[1] = vertex.normal[1];
                    binormals[2] = vertex.normal[2];
                }
                else
                {
                    binormals[0] = vertex.binormal[0];
                    binormals[1] = vertex.binormal[1];
                    binormals[2] = vertex.binormal[2];
                }
                myfile << std::fixed << std::setprecision(8);
                myfile << vertex.position[0]    << " " << vertex.position[1]    << " " << vertex.position[2]    << " "
                       << vertex.normal[0]      << " " << vertex.normal[1]      << " " << vertex.normal[2]      << " "
                       << tangents[0]           << " " << tangents[1]           << " " << tangents[2]           << " "
                       << binormals[0]          << " " << binormals[1]          << " " << binormals[2]          << " "
                       << vertex.uv[0]          << " " << vertex.uv[1]          << " "
                       << 0.00000000f           << " " << 0.00000000f           << " "
                       << 0.00000000f           << " " << 0.00000000f           << " "
                       << boneIds[0]            << " " << boneIds[1]            << " " << boneIds[2] << " " << boneIds[3] << " 0 0 0 0 "
                       << vertex.boneWeights[0] << " " << vertex.boneWeights[1] << " " << vertex.boneWeights[2] << " " << vertex.boneWeights[3] << " 0 0 0 0 " << endl;

                delete [] tangents;
                delete [] binormals;
                delete[] boneIds;
            }

            // Write Triangles
            myfile << "triangles" << endl;
            for (unsigned int ti = 0; ti < grannyMeshInfo.triangles.size(); ti++){
                int * triangle = grannyMeshInfo.triangles[ti];
                myfile << triangle[0] << " " << triangle[1] << " " << triangle[2] << " 0" << endl;
            }
        }
    }
    myfile <<  "end" << endl;
    myfile.close();
    WriteModelToCN6Response response(decalMeshNames, textureMaps);
    return response;
}

void Exporter::resetMemory(){

}
