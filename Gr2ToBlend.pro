QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    NBCpp/BiLookup.cpp \
    NBCpp/GrannyAnimationWrapper.cpp \
    NBCpp/GrannyBoneInfo.cpp \
    NBCpp/GrannyBoneWrapper.cpp \
    NBCpp/GrannyFileWrapper.cpp \
    NBCpp/GrannyMaterialWrapper.cpp \
    NBCpp/GrannyMeshInfo.cpp \
    NBCpp/GrannyMeshVertexStructInfo.cpp \
    NBCpp/GrannyMeshWrapper.cpp \
    NBCpp/GrannyModelInfo.cpp \
    NBCpp/GrannyModelWrapper.cpp \
    NBCpp/GrannySkeletonInfo.cpp \
    NBCpp/GrannySkeletonWrapper.cpp \
    NBCpp/GrannyTransformInfo.cpp \
    NBCpp/GrannyTransformWrapper.cpp \
    NBCpp/GrannyVertexInfo.cpp \
    NBCpp/IndieAnimDef.cpp \
    NBCpp/IndieMaterial.cpp \
    NBCpp/Matrice.cpp \
    NBCpp/NumberUtils.cpp \
    NBCpp/writemodeltocn6response.cpp \
    exporter.cpp \
    fileconfig.cpp \
    main.cpp \
    mainwindow.cpp \

HEADERS += \
    NBCpp/BiLookup.h \
    NBCpp/GrannyAnimationWrapper.h \
    NBCpp/GrannyBoneInfo.h \
    NBCpp/GrannyBoneWrapper.h \
    NBCpp/GrannyFileWrapper.h \
    NBCpp/GrannyMaterialWrapper.h \
    NBCpp/GrannyMeshInfo.h \
    NBCpp/GrannyMeshVertexStructInfo.h \
    NBCpp/GrannyMeshWrapper.h \
    NBCpp/GrannyModelInfo.h \
    NBCpp/GrannyModelWrapper.h \
    NBCpp/GrannySkeletonInfo.h \
    NBCpp/GrannySkeletonWrapper.h \
    NBCpp/GrannyTransformInfo.h \
    NBCpp/GrannyTransformWrapper.h \
    NBCpp/GrannyVertexInfo.h \
    NBCpp/IndieAnimDef.h \
    NBCpp/IndieMaterial.h \
    NBCpp/Matrice.h \
    NBCpp/NumberUtils.h \
    NBCpp/granny.h \
    NBCpp/writemodeltocn6response.h \
    exporter.h \
    fileconfig.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

LIBS += C:\Travail\Merlo\Jeu\Gr2ToBlend\granny2.lib
INCLUDEPATH += C:\Travail\Merlo\Jeu\Gr2ToBlend\NBCpp
## Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target




